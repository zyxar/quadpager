/******************************************************************************
 * Copyright (c) 2012 by Marcus Zy                                            *
 * zyxar.com                                                                  *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/

#include "pagemap.h"

pagemap::pagemap(int n) {
    _size = n;
    if (n % 4 != 0) {
        _size += (4 - n%4) % 4;
    }
    page[0] = _size;
    page[1] = _size - 3;
    page[_size/2] = 2;
    page[_size/2 + 1] = 3;
    for (int i = 1; i <= _size/4; ++i) {
        page[2*i] = _size - 4*i;
        page[1 + 2*i] = _size - 4*i - 3;
    }
    for (int i = 0; i < _size/4; ++i) {
        page[_size/2 + 2*i] = 2 + 4*i;
        page[_size/2 + 2*i + 1] = 3 + 4*i;
    }
}

int
pagemap::pagenum(int cur) {
    return page[cur];
}

int 
pagemap::operator[](int cur) {
    return page[cur];
}
