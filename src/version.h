#ifndef __VERSION_H__
#define __VERSION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"
#include <cstdio>

#if defined(__DATE__) && defined(__TIME__)
#define HAVE_DATE_TIME 1
#endif

#define _VERSION_LONG "quadpager 1.0 (15 March 2012)"
#define _ORGANISATION "© 2009 - 2012 ZyXar.com"

#if defined(HAVE_DATE_TIME) || defined(PROTO)
# if (defined(VMS) && defined(VAXC)) || defined(PROTO)
char longVersion[sizeof(__DATE__) + sizeof(__TIME__) + 11];

void
make_version(void)
{
    strcpy(longVersion, "compiled ");
    strcat(longVersion, __DATE__);
    strcat(longVersion, " ");
    strcat(longVersion, __TIME__);
}
# else
const char *longVersion = "compiled " __DATE__ " " __TIME__;
# endif
#else
const char *longVersion = _VERSION_LONG;
#endif

void quad_version(void) {
    #if defined(HAVE_DATE_TIME) && defined(VMS) && defined(VAXC)
        make_version(); /* Construct the long version string. */
    #endif

    fprintf(stderr, "%s %s\n", _VERSION_LONG, _ORGANISATION);
    fprintf(stderr, "%s %s\n", _PLATFORM_, longVersion);
    fprintf(stderr, "compiler: %s (%s)\n\n", _THE_COMPILER_, __VERSION__);
}

#ifdef __cplusplus
}
#endif

#endif