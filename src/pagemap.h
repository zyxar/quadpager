/******************************************************************************
 * Copyright (c) 2012 by Marcus Zy                                            *
 * zyxar.com                                                                  *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/

#ifndef _PAGEMAP_H_
#define _PAGEMAP_H_

#include <iostream>
#include <map>
#include <string>
#include <cstdlib>

class pagemap {
public:
    pagemap() {}
    pagemap(int n);
    std::map<int, int> const& pages() const { return page; }
    int size() const { return _size; }
    int pagenum(int);
    int operator[](int);

private:
    std::map<int, int> page;
    int _size;
/*
    std::string int2str(const int& i) {
        std::string ret;
        std::stringstream ss(ret);
        ss << i;
        return ss.str();
    }
    inline static int level(int num, int base) {
        int ret = 1;
        while ((num = num/base) >= base) ++ret;
        if (num >= 1) ++ret;
        return ret;
    }
    inline static int level(int num) {
        return level(num, 10);
    }
*/
};

#endif