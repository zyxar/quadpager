/******************************************************************************
 * Copyright (c) 2012 by Marcus Zy                                            *
 * zyxar.com                                                                  *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/
 
#include <podofo/podofo.h>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/sysctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <signal.h>
#include <err.h>
#include "pagemap.h"
#include "version.h"

using namespace PoDoFo;

const char* prog = NULL;
static char decimal_point;
static struct timeval before;
sig_atomic_t spinning = true;

void spin() {
    int i = 0;
    while(spinning){
        fprintf(stderr, "=>\x08");
        if (++i % 80 == 0) putchar('\n');
        sleep(1);
    }
    fprintf(stderr, "\n");
}

void sigchld(int) {
    spinning = false;
}

static void
showtime(struct timeval *before, struct timeval *after, struct rusage *ru) {
    after->tv_sec -= before->tv_sec;
    after->tv_usec -= before->tv_usec;
    if (after->tv_usec < 0) after->tv_sec--, after->tv_usec += 1000000;
    double Ctime = ((double)(ru->ru_utime.tv_sec + ru->ru_stime.tv_sec))*1000 + ((double)(ru->ru_utime.tv_usec + ru->ru_stime.tv_usec))/1000; // in ms
    double Ttime = (double)(after->tv_sec * 1000 + ((double)after->tv_usec) / 1000); // in ms
    fprintf(stderr, "process: %ld%c%02ds real ", after->tv_sec, decimal_point, after->tv_usec/10000);
    fprintf(stderr, "%ld%c%02ds user ", ru->ru_utime.tv_sec, decimal_point, ru->ru_utime.tv_usec/10000);
    fprintf(stderr, "%ld%c%02ds sys ", ru->ru_stime.tv_sec, decimal_point, ru->ru_stime.tv_usec/10000);
    fprintf(stderr, "%d%% cpu\n", (int)(100 * (Ctime / Ttime)));
}

static void
siginfo(int) {
    struct rusage ru;
    struct timeval after;
    getrusage(RUSAGE_CHILDREN, &ru);
    showtime(&before, &after, &ru);
}

bool process(const char* srcfile, const char* destfile, int offset) throw (PdfError) {
    PdfMemDocument input( srcfile );
    int n = input.GetPageCount() - offset;
    //fprintf (stderr, "page number of %s: %d.\n", src, n);
    if (n < 4) {
        fprintf(stderr, "Error: too few pages.\n");
        return false;
    }
    pagemap pages(n);
    PdfMemDocument dest;
    dest.SetPdfVersion(ePdfVersion_1_6);
    PdfMemDocument blankf;
    blankf.SetPdfVersion(ePdfVersion_1_6);
    PdfRect rect = input.GetPage(offset + 1)->GetPageSize();
    blankf.CreatePage(rect);
    for (int i = 0; i < pages.size(); ++i) {
        if (pages[i] > n) dest.InsertPages(blankf, 0, 1);
        else dest.InsertPages(input, pages[i] - 1 + offset, 1);
    }
    dest.Write(destfile);
    return true;
}

void
_usage(void) {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, " %s [options] <input.pdf> <output.pdf> [<offset in input>]\n\n", prog);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, " -b, --verbose                 Print libPoDoFo debug info when in processing.\n");
    fprintf(stderr, " -l, --log LOGPATH             Redirect debug info to specified file.\n\n");
}

int main(int argc, const char *argv[]) {
    register char* cp;
    if (argv[0] == NULL) prog = "quadpager";
    else if ((cp = strrchr(argv[0], '/')) != NULL) prog = cp + 1;
    else prog = argv[0];

    static struct option longopts[] = {
        { "verbose", no_argument, NULL, 'b' },
        { "log", required_argument, NULL, 'l' },
        { "version", no_argument, NULL, 'v' },
        { NULL, 0, NULL, 0 }
    };

    static int oc;
    const char* logfile = "/dev/null";
    extern char* optarg;
    extern int optind;
    bool verbose = false;
    int status;
    struct rusage usage;
    struct timeval after;
    struct rlimit rl;

    (void) setlocale(LC_NUMERIC, "");
    decimal_point = localeconv()->decimal_point[0];

    while((oc = getopt_long(argc, (char*const*)argv, "bl:v", longopts, NULL)) != -1) {
        switch(oc){
            case 'l':
                logfile = optarg;
                break;
            case 'b':
                verbose = true;
                break;
            case 'v':
                quad_version();
                exit(1);
            default:
                _usage();
                exit(1);
        }
    }

    argc -= optind;
    argv += optind;

    const char* src;
    const char* dest;

    int offset = 0;
    if ( argc < 2 ) {
        _usage();
        return -1;
    }
    src = argv[0];
    dest = argv[1];
    if ( argc > 2) {
        char* endptr = NULL;
        offset = strtol(argv[2], &endptr, 10);
        if (endptr[0] != 0 || offset < 0) {
            fprintf(stderr, "Oops... Invalid offset: %s.\n", endptr);
            _usage();
            return 1;
        }
    }

    gettimeofday(&before, (struct timezone *)NULL);

    pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(-1);
    }
    if (pid == 0) {
        int fd, ret = 0;
        if (!verbose) {
            // supress the unwanted debug info from libPoDoFo.
            fd = open(logfile, O_WRONLY); 
            dup2(fd, 2); // fd would gain 2, expected stderr.
        }
        try {
            process(src, dest, offset);
        } catch ( PdfError &e ) {
            fprintf (stderr, "Oops... Error %i occurred!\n", e.GetError());
            e.PrintErrorMsg();
            ret = e.GetError();
        }
        if (!verbose) close(fd);
        exit(ret);
    }

    (void)signal(SIGINT, SIG_IGN);
    (void)signal(SIGQUIT, SIG_IGN);
    (void)signal(SIGINFO, siginfo);
    (void)signal(SIGCHLD, sigchld);

    spin(); // wait for signal_child
    gettimeofday(&after, (struct timezone *)NULL);
    
    wait4(pid, &status, WUNTRACED, &usage);
    if ( !WIFEXITED(status) || WEXITSTATUS(status) != 0)
        warnx("worker process terminated abnormally");
    int estat = WIFSIGNALED(status) ? WTERMSIG(status) : 0;
    showtime(&before, &after, &usage);

    if (estat) { // set signal, but avoid core dumping.
        if (signal(estat, SIG_DFL) == SIG_ERR)
            warn("signal");
        else {
            rl.rlim_max = rl.rlim_cur = 0;
            if (setrlimit(RLIMIT_CORE, &rl) == -1)
                warn("setrlimit");
            kill(getpid(), estat);
        }
    }

    return WIFEXITED(status) ? WEXITSTATUS(status) : EXIT_FAILURE;
}
